import logging

import openhab.items
import logging
from neomodel import db

from app.graph_database.neo_orm import Device, Location, State, DeviceGroup, OpenhabGroup, OpenhabItem

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


class NeoService:
    def __init__(self, openhabPlatform):
        self.logger = logging.getLogger("main-logger")
        self.openhab = openhabPlatform

    @staticmethod
    def delete_all_nodes_and_relationships():
        db.cypher_query(
            '''
            MATCH (n)
            DETACH DELETE n
            '''
        )

    def _get_nodes(self, node_type):
        return node_type.nodes.all()

    def _get_or_create(self, node_type, name):
        node = node_type.nodes.get_or_none(name=name)
        if not node:
            self.logger.info(f"Creating {node_type}: {name}")
            node = node_type(name=name).save()
        return node

    def _get_or_create_device(self, name, device_type):
        node = self._get_or_create(OpenhabItem, name)
        if not isinstance(node, Device):
            self.logger.info(f"add Device label to node id({node.id}) {node.name}")
            node.cypher(f"MATCH (a) WHERE id(a)=$self SET a:Device SET a.type_='{device_type}'")
            node.refresh()
            node = self._get_node(Device, name)
            self.logger.info(f'Labels of {node.name} are: {node.inherited_labels()}')
        return node

    def _get_or_create_location(self, name):
        node = self._get_node(OpenhabGroup, name=name)
        if node:
            if isinstance(node, Location):
                return node
            else:
                self.logger.warning(f"add Location label to node {node.name}")
                node.cypher("MATCH (a) WHERE id(a)=$self SET a:Location")
        else:
            node = self._get_or_create(Location, name=name).save()
        return node

    def _get_or_create_device_group(self, name):
        node = self._get_node(OpenhabGroup, name=name)
        if node:
            if isinstance(node, DeviceGroup):
                return node
            else:
                self.logger.warning(f"add DeviceGroup label to node {node.name}")
                node.cypher("MATCH (a) WHERE id(a)=$self SET a:DeviceGroup")
        else:
            node = self._get_or_create(DeviceGroup, name=name).save()
        return node

    def _get_node(self, node_type, name):
        return node_type.nodes.get_or_none(name=name)

    def _connect_node_to_group(self, node, group_names: list):
        for name in group_names:
            group = self._get_or_create(OpenhabGroup, name)
            if group:
                self.logger.info(f"Connect node id({node.id}) {node.name} to {name}")
                node.group.connect(group)

    def update_device_state(self, device_name, state_type, state_value):
        device = self.get_device(device_name)
        state = self.get_device_state(device_name)
        if state.value == state_value and state.type_ == state_type:
            return state
        new_state = State(value=state_value, type_=state_type).save()
        device.state.connect(new_state)
        return new_state

    def get_devices_all(self):
        return self._get_nodes(Device)

    def get_device(self, name):
        return self._get_node(Device, name)

    def get_device_state(self, device_name):
        device = self._get_node(Device, name=device_name)
        state = device.state.single()
        if not state:
            state = State(value='NULL', type_='INIT').save()
            device.state.connect(state)
        return state

    def get_device_location(self, device_name):
        device = self._get_node(Device, name=device_name)
        location = device.location.single()
        return location

    def get_locations_all(self):
        return self._get_nodes(Location)

    def get_location(self, name):
        return self._get_node(Location, name)

    def get_device_groups_all(self):
        return self._get_nodes(DeviceGroup)

    def get_device_group(self, name):
        return self._get_node(DeviceGroup, name)

    def get_group_members(self, group_name):
        return self.get_device_group(group_name).member.all()

    def add_item(self, item: openhab.items.Item):
        if item.group:
            is_location = any(tag in self.openhab.location_list for tag in item.tags)
            if is_location:
                self.logger.info(f"{item.name} is Location")
                group = self._get_or_create_location(item.name)
            else:
                group = self._get_or_create_device_group(item.name)
            self._connect_node_to_group(group, group_names=item.groupNames)
        else:
            self.logger.info(f"State: {item.state}, Unit:{item.unit_of_measure}")
            device = self._get_or_create_device(item.name, item.type_)
            self._connect_node_to_group(device, group_names=item.groupNames)
            if not device.state:
                s = State(value=item.state, type_='').save()
                device.state.connect(s)

    def get_devices_by_location(self, location_name):
        location = self.get_location(location_name)
        members = location.member.all()
        devices = []

        def _helper(node):
            if isinstance(node, Device):
                devices.append(node)
            elif isinstance(node, DeviceGroup):
                for group_member in node.member:
                    _helper(group_member)

        for member in members:
            _helper(member)
        return devices

    def get_devices_by_locataion_and_type(self, location_name, device_type):
        devices_by_location = self.get_devices_by_location(location_name)
        return list(filter(lambda device: device.type_ == device_type, devices_by_location))

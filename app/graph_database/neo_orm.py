from neomodel import StructuredNode, StringProperty, ArrayProperty, UniqueProperty, RelationshipTo, RelationshipFrom, \
    DateTimeProperty, StructuredRel


class OpenhabItem(StructuredNode):
    # __abstract_node__ = True
    name = StringProperty(unique_index=True, required=True)
    group = RelationshipFrom('OpenhabItem', 'member')


class OpenhabGroup(OpenhabItem):
    # __abstract_node__ = True
    member = RelationshipTo('OpenhabItem', 'member')


class StateRel(StructuredRel):
    since = DateTimeProperty(default_now=True)


class DeviceGroup(OpenhabGroup):
    type_ = StringProperty(required=False)
    label = StringProperty(required=False)


class Device(OpenhabItem):
    # Number, Contact, DateTime, Rollershutter, Color, Dimmer, Switch, Player
    type_ = StringProperty(required=True)
    label = StringProperty(required=False)
    state = RelationshipTo('State', 'haveState', model=StateRel)
    location = RelationshipTo('Location', 'inLocation')
    category = StringProperty(required=False)
    # tags = ArrayProperty(required=False)
    # quantity_type: StringProperty(required=False)
    # tags: StringProperty(required=False)
    # group = RelationshipFrom('DeviceGroup', 'member')
    # group_names: StringProperty(required=False)
    # group_type: StringProperty(required=False)
    # function_name: StringProperty(required=False)
    # function_params: ArrayProperty(required=False)


class Location(OpenhabGroup):
    label = StringProperty(required=False)
    device = RelationshipFrom('Device', 'inLocation')


class State(StructuredNode):
    value = StringProperty(required=True)
    type_ = StringProperty(required=True)

import os
import configparser

config = configparser.ConfigParser()
config.read('properties.ini')
db_properties = config['DB']
db_user = db_properties['user']
db_password = db_properties['password']
db_server_uri = db_properties.get('uri', os.getenv('NEO4J_ADDRESS', 'localhost'))
db_bolt_url = f'bolt://{db_user}:{db_password}@{db_server_uri}:7687'
db_aura_url = f'neo4j+s://{db_user}:{db_password}@{db_server_uri}'

oh_properties = config['OpenHab']
oh_server_uri = os.getenv('OH_ADDRESS', oh_properties.get('uri', 'localhost'))
oh_server_port = os.getenv('OH_PORT', oh_properties.get('port', '8080'))
oh_base_url = os.getenv('OH_URL', f'http://{oh_server_uri}:{oh_server_port}/rest')
oh_username = os.getenv('OH_USER', oh_properties.get('user', 'admin'))
oh_password = os.getenv('OH_PASS', oh_properties.get('password', 'admin'))

log_properties = config['Logger']
LOG_LEVEL = os.getenv('LOG_LEVEL', log_properties.get('level', 'DEBUG'))

logger_config = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "default": {
            "format": "[%(asctime)s] %(levelname)s - %(message)s",
            "datefmt": "%Y-%m-%d %H:%M:%S"

        },
    },
    "handlers": {
        "default": {
            "formatter": "default",
            "class": "logging.StreamHandler",
            "stream": "ext://sys.stderr",
        },
    },
    "loggers": {
        "main-logger": {"handlers": ["default"], "level": LOG_LEVEL},
        "node-red-logger": {"handlers": ["default"], "level": LOG_LEVEL}
    },
}

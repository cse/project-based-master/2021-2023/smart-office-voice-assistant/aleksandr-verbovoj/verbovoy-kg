import logging
import json
from types import SimpleNamespace
from openhab.items import Item

from app.graph_database.neo_services import NeoService


class NodeRedRequestHandler:
    def __init__(self, neo_service: NeoService, openhab):
        self.neo_service = neo_service
        self.openhab_service = openhab
        self.logger = logging.getLogger("node-red-logger")

    def main_handler(self, data):
        message = data.decode('utf-8')
        if not message:
            return
        self.logger.info(f"data from node_red: {message}")
        msg = json.loads(message, object_hook=lambda x: SimpleNamespace(**x))

        self.logger.info(msg.payload)
        self.logger.info(msg.type)
        self.logger.info(msg.item_name)
        if msg.type == "ItemStateChangedEvent":
            return self.change_state(msg.item_name, msg.payload)
        elif msg.type == "ItemAddedEvent":
            return self.add_item(msg.item_name)
        elif msg.type == "ItemRemovedEvent":
            return self.remove_item(msg.item_name)

    def change_state(self, device_name, payload):
        state_type = payload.type
        state_value = payload.value
        return self.neo_service.update_device_state(device_name, state_type=state_type, state_value=state_value)

    def add_item(self, item_name):
        item = self.openhab_service.get_item(item_name=item_name)
        self.neo_service.add_item(item)

    def remove_item(self, item_name):
        pass
        # self.neo_service.set_item_tag(item_name, "REMOVED")

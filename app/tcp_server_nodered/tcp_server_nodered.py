import socketserver
import threading


class SingleTCPHandler(socketserver.BaseRequestHandler):
    def __init__(self, callback, request, client_address, server):
        self.callback = callback
        super().__init__(request, client_address, server)

    def handle(self):
        data = self.request.recv(1024).strip()
        self.callback(data)


def say_hi(data):
    print(f"FROM CALLBACK:{data}")


def handler_factory(callback):
    def create_handler(*args, **keys):
        return SingleTCPHandler(callback, *args, **keys)

    return create_handler


def node_red_server_start(host, port, handler_callback):
    with socketserver.ThreadingTCPServer((host, port), handler_factory(handler_callback)) as server:
        server.serve_forever()


if __name__ == '__main__':
    HOST, PORT = '127.0.0.1', 9999
    server_thread = threading.Thread(target=node_red_server_start, args=(HOST, PORT, say_hi))
    server_thread.daemon = True
    server_thread.start()

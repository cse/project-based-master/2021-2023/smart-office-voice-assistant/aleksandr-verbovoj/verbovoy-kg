from typing import List

from pydantic import BaseModel, Field


class Device(BaseModel):
    name: str
    type_: str
    state: str
    group: str

    @staticmethod
    def from_value(device):
        return Device(name=device.name, type_=device.type_)


class Devices(BaseModel):
    devices: List[Device] = []


class Location(BaseModel):
    name: str = Field(example="Room313")
    label: str = Field(example="Комната 313")


class Locations(BaseModel):
    locations: List[Location]


class UpdateStateByLocationAndTypeDTO(BaseModel):
    location_name: str
    device_type: str
    command: str


class ChangeStateDTO(BaseModel):
    oldType: str
    oldValue: str
    type: str
    value: str


class NeoCreate(BaseModel):
    name: str
    type_: str

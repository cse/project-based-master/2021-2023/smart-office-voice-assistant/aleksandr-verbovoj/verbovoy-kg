from app.DTOs.dto import UpdateStateByLocationAndTypeDTO
from fastapi import status
from fastapi.responses import JSONResponse

from app.graph_database.neo_services import NeoService
from app.openhab_platform.openhab_service import OpenhabService


class RestService:
    def __init__(self, openhab_service: OpenhabService, neo_service: NeoService):
        self.openhab_service = openhab_service
        self.neo_service = neo_service

    def get_locations(self):
        locations = self.neo_service.get_locations_all()
        return {'locations': [location.name for location in locations]}

    def get_location_info(self, location_name):
        location = self.neo_service.get_location(name=location_name)
        devices = self.neo_service.get_devices_by_location(location_name=location_name)
        return {'name': location.name, 'label': location.label, 'devices': [device.name for device in devices]}

    def get_groups_all(self):
        groups = self.neo_service.get_device_groups_all()
        return {'groups': [group.name for group in groups]}

    def get_group_info(self, group_name):
        group = self.neo_service.get_device_group(group_name)
        members = self.neo_service.get_group_members(group_name)
        return {'name': group.name, 'members': [member.name for member in members]}

    def get_group_members(self, group_name):
        members = self.neo_service.get_group_members(group_name)
        return {'members': [member.name for member in members]}

    def update_devices_state_by_location_and_type(self, update_state: UpdateStateByLocationAndTypeDTO):
        location = self.neo_service.get_location(name=update_state.location_name)
        if location is None:
            return JSONResponse(status_code=status.HTTP_204_NO_CONTENT, content={'message': 'Location Not Found'})
        devices_to_change_state = self.neo_service.get_devices_by_locataion_and_type(location_name=update_state.location_name,
                                                                                     device_type=update_state.device_type)
        if not devices_to_change_state:
            return JSONResponse(status_code=status.HTTP_204_NO_CONTENT, content={'message': 'Devices Not Found'})
        for device in devices_to_change_state:
            self.openhab_service.send_item_command(item_name=device.name, command=update_state.command)
        return {'devices': [device.name for device in devices_to_change_state]}

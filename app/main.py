import logging
import threading

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from starlette import status
from starlette.responses import JSONResponse

from app.DTOs.dto import UpdateStateByLocationAndTypeDTO, NeoCreate, ChangeStateDTO, Devices, Device, Locations, \
    Location
from app.openhab_platform.openhab_service import OpenhabService
from app.openhab_platform.openhab_platform import OpenhabPlatform
from app.graph_database.neo_services import NeoService
from neomodel import config
from app.restservice import RestService
from app.tcp_server_nodered.node_red_server_handler import NodeRedRequestHandler
from app.tcp_server_nodered.tcp_server_nodered import node_red_server_start
from config import oh_base_url, oh_username, oh_password, db_bolt_url, db_aura_url, logger_config

from logging.config import dictConfig
import uvicorn

dictConfig(logger_config)
logger = logging.getLogger("main-logger")

config.DATABASE_URL = db_aura_url
app = FastAPI()

origins = [
    "http://localhost",
    "http://localhost:3001",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/")
async def root():
    return {"message": 'Hello World'}


@app.get("/items")
async def get_items():
    items = openhab_service.openhabPlatform.get_all_items().items()
    for name, item in items:
        logger.info(f"doing {name}, {item}")
        neo_service.add_item(item)
    devices = list(map(lambda device: device.name, neo_service.get_devices_all()))
    logger.info(f"Items to show: {devices}")

    return devices


@app.get("/devices")
async def get_devices():
    devices = neo_service.get_devices_all()
    response = {
        'devices': [{'name': device.name, 'type_': device.type_, 'group': device.group.single().name,
                     'state': {'value': neo_service.get_device_state(device.name).value,
                               'type_': neo_service.get_device_state(device.name).type_}} for device in devices]}
    return response


@app.get("/devices/{device_name}")
async def get_device_info(device_name: str):
    device = neo_service.get_device(device_name)
    state = neo_service.get_device_state(device_name)
    response = Device.from_value(device)
    response.state = state
    return response


@app.get("/groups")
async def get_groups_all():
    groups = neo_service.get_device_groups_all()
    return {'groups': [group.name for group in groups]}


@app.get("/groups/{group_name}")
async def get_group_info(group_name: str):
    return rest_service.get_group_info(group_name=group_name)


@app.get('/groups/{group_name}/members')
async def get_group_members(group_name: str):
    return rest_service.get_group_members(group_name=group_name)


@app.get("/locations")
async def get_all_locations():
    return rest_service.get_locations()


@app.get("/locations/{location_name}", response_model=Location)
async def get_location_info(location_name: str):
    return rest_service.get_location_info(location_name=location_name)


@app.post("/update")
async def update_state_of_device_in_group_by_location_and_type(update_state: UpdateStateByLocationAndTypeDTO):
    location = neo_service.get_location(name=update_state.location_name)
    if not location:
        return JSONResponse(status_code=status.HTTP_204_NO_CONTENT, content={'message': 'Location Not Found'})
    devices_to_change_state = neo_service.get_devices_by_locataion_and_type(location_name=update_state.location_name,
                                                                            device_type=update_state.device_type)
    if not devices_to_change_state:
        return JSONResponse(status_code=status.HTTP_204_NO_CONTENT, content={'message': 'Devices Not Found'})
    for device in devices_to_change_state:
        openhab_service.send_item_command(item_name=device.name, command=update_state.command)
    return {'devices': [device.name for device in devices_to_change_state]}


@app.post("/neo/create", include_in_schema=False)
async def create_item_neo(neo_create: NeoCreate):
    return {"message": "Item " + str(neo_create.name) + "not created"}


@app.put("/neo/{item_name}/update", include_in_schema=False)
async def change_item_state(item_name: str, change_state: ChangeStateDTO):
    new_state = neo_service.update_device_state(device_name=item_name, state_type=change_state.type,
                                                state_value=change_state.value)
    return {'state': new_state, 'type': new_state.type_}


if __name__ == '__main__':
    openhab = OpenhabPlatform(oh_base_url, username=oh_username, password=oh_password)
    openhab_service = OpenhabService(openhabPlatform=openhab)
    neo_service = NeoService(openhabPlatform=openhab)
    rest_service = RestService(openhab_service=openhab_service, neo_service=neo_service)
    # config.DATABASE_URL = db_aura_url
    node_red_request_handler = NodeRedRequestHandler(neo_service=neo_service, openhab=openhab)

    HOST, PORT = '127.0.0.1', 9999
    server_thread = threading.Thread(target=node_red_server_start,
                                     args=(HOST, PORT, node_red_request_handler.main_handler))
    # server_thread.daemon = True
    server_thread.start()
    uvicorn.run(app, host="0.0.0.0", port=8082)

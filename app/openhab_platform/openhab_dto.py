from typing import Union, Optional


class ItemModel:
    def __init__(self, name: str, type_: str,
                 quantityType: Optional[str] = None,
                 label: Optional[str] = None,
                 category: Optional[str] = None,
                 tags: Optional[list[str]] = None,
                 group_names: Optional[list[str]] = None,
                 group_type: Union[str, None] = None,
                 function_name: Optional[str] = None,
                 function_params: Optional[list[str]] = None):
        self.name = name
        self.type_ = type_
        self.quantityType = quantityType
        self.label = label
        self.category = category
        self.tags = tags
        self.group_names = group_names
        self.group_type = group_type
        self.function_name = function_name
        self.function_params = function_params

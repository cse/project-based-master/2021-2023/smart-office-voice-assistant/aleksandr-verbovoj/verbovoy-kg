import openhab.items
from openhab import OpenHAB


class OpenhabPlatform:
    def __init__(self, base_url, username=None, password=None, location_list=None):
        if location_list is None:
            location_list = ['Office', 'Room']
        self.openhab = OpenHAB(base_url=base_url, username=username, password=password)
        self.location_list = location_list

    def get_all_items(self):
        return self.openhab.fetch_all_items()

    def get_item(self, item_name) -> openhab.items.Item:
        return self.openhab.get_item(item_name)

    def get_item_raw(self, item_name):
        return self.openhab.get_item_raw(item_name)

    def get_rules(self):
        rules = self.openhab.rules.get()
        return {'rules': rules}

    def create_item(self, item: dict):
        self.openhab.create_or_update_item(name=item['name'],
                                           _type=item['type_'],
                                           quantity_type=item['quantityType'],
                                           label=item['label'],
                                           category=item['category'],
                                           tags=item['tags'],
                                           group_names=item['groupNames'],
                                           function_name=item['function_name'],
                                           function_params=item['function_params'])

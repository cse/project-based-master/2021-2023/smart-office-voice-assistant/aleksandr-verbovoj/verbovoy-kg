import json

from app.openhab_platform.openhab_platform import OpenhabPlatform


class OpenhabService:

    def __init__(self, openhabPlatform: OpenhabPlatform):
        self.openhabPlatform = openhabPlatform

    def delete_all_items(self):
        items = self.openhabPlatform.get_all_items().items()
        for name, item in items:
            self.openhabPlatform.openhab.session.delete(self.openhabPlatform.openhab.url_rest + '/items/' + name)

    def add_items_from_json(self, json_data):
        data = json.loads(json_data)
        all_items = [self.openhabPlatform.openhab.json_to_item(item) for item in data]
        items = list(filter(lambda item: not item.group, all_items))
        groups = list(filter(lambda item: item.group, all_items))
        for item in items:
            self.openhabPlatform.create_item(item.__dict__)
        for group in groups:
            group.type_ = 'Group'
            self.openhabPlatform.create_item(group.__dict__)

    def send_item_command(self, item_name, command):
        item = self.openhabPlatform.get_item(item_name)
        item.command(command)

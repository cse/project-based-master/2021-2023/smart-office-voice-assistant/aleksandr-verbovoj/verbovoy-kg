# Сервис для работы с OpenHAB и Neo4j через HTTP запросы

# Первый запуск с помощью Docker compose

1) `docker compose -f docker-compose.yml up -d`
2) Подождать **2 - 5** минут, пока скачиваются образы и устанавливаются зависимости
    1) порты для **OpenHAB** : _8080_, _8443_
    2) порты для **Neo4j** : _7687_, _7474_
    3) **nodered**: 1880
    4) **uvicorn python сервер** : _8082_
3) Openhab: Перейти на http://localhost:8080 (подождать если не отвечает)
4) Ввести Username: _salix_26_ , Password: _itmoflex313_
    1) (Можно вводить любой, но тогда поменять данные `oh_username` и `oh_password` в [app/const.py](app/config.py)
       соответсвенно)
5) Пропустить установку (кнопка `Skip Setup`)
6) http://localhost:8080/createApiToken - создание API token для сервиса.
    1) Имя пользователя: **salix_26**
    2) Пароль: **itmoflex313**
    3) Имя - любое
    4) Поменять токен в файле [properties.ini](app/properties.ini) **12** строчка user=**oh.service.4LsV...**
7) **Импорт данных**: скопировать файл [org.openhab.core.items.Item.json](app/org.openhab.core.items.Item.json) и вставить с
   заменой по пути Verbovoy_KG/openhab/userdata/jsondb
8) http://localhost:8080/createApiToken - создание API token для nodered
    1) Имя пользователя: **salix_26**
    2) Пароль: **itmoflex313**
    3) Имя токена: **nodered**
    4) Поменять токен в файле [nodered-import.json](nodered-import.json) **170** строчка _oh.nodered.DxK..._
9) http://localhost:1880/ -> (cmd/ctrl-i) -> вставить содержание [nodered-import.json](nodered-import.json)
10) http://localhost:8082/docs - для просмотра схемы запросов

# Последующие запуски Docker compose

`docker compose -f docker-compose.yml up -d --build`
